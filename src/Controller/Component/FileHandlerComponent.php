<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * FileHandler component
 */
class FileHandlerComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    
    // The other component your component uses
    public $components = ['FileNameHandler'];

    public function uploadImage($file = null, $destination = 'default')
    {
    	$return = ['status'=>'fail', 'message'=>'status', 'file_name'=>'', 'url'=>''];

    	if($file != null)
    	{
    		$fileNameHandler = $this->FileNameHandler->fileNameHandlerFunction($file['name']);

    		$arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions

            //only process if the extension is valid
            if(in_array($fileNameHandler['ext'], $arr_ext))
            {
            	move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $destination . '/'. $fileNameHandler['rand_name']);

            	$return['status'] 	= 'success';
            	$return['message'] 	= 'Image Uploaded Succesfully.';
            	$return['file_name']= $fileNameHandler['rand_name'];
            }
            else
            {
            	$return['message'] 	= 'Image Ext should be jpg, jpeg, gif.';
            }
    	}
    	else
    	{
    		$return['message'] 	= 'Funtion Required at least One Parameter.';
    	}

    	return $return;
    }
}
