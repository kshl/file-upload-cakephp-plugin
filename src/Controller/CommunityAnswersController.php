<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CommunityAnswers Controller
 *
 * @property \App\Model\Table\CommunityAnswersTable $CommunityAnswers
 */
class CommunityAnswersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Communities', 'CommunityQuestions', 'ParentCommunityAnswers']
        ];
        $communityAnswers = $this->paginate($this->CommunityAnswers);

        $this->set(compact('communityAnswers'));
        $this->set('_serialize', ['communityAnswers']);
    }

    /**
     * View method
     *
     * @param string|null $id Community Answer id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $communityAnswer = $this->CommunityAnswers->get($id, [
            'contain' => ['Users', 'Communities', 'CommunityQuestions', 'ParentCommunityAnswers', 'CommunityAnswerApprovals', 'CommunityAnswerLikes', 'ChildCommunityAnswers']
        ]);

        $this->set('communityAnswer', $communityAnswer);
        $this->set('_serialize', ['communityAnswer']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $communityAnswer = $this->CommunityAnswers->newEntity();
        if ($this->request->is('post')) {
            $communityAnswer = $this->CommunityAnswers->patchEntity($communityAnswer, $this->request->getData());
            if ($this->CommunityAnswers->save($communityAnswer)) {
                $this->Flash->success(__('The community answer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The community answer could not be saved. Please, try again.'));
        }
        $users = $this->CommunityAnswers->Users->find('list', ['limit' => 200]);
        $communities = $this->CommunityAnswers->Communities->find('list', ['limit' => 200]);
        $communityQuestions = $this->CommunityAnswers->CommunityQuestions->find('list', ['limit' => 200]);
        $parentCommunityAnswers = $this->CommunityAnswers->ParentCommunityAnswers->find('list', ['limit' => 200]);
        $this->set(compact('communityAnswer', 'users', 'communities', 'communityQuestions', 'parentCommunityAnswers'));
        $this->set('_serialize', ['communityAnswer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Community Answer id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $communityAnswer = $this->CommunityAnswers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $communityAnswer = $this->CommunityAnswers->patchEntity($communityAnswer, $this->request->getData());
            if ($this->CommunityAnswers->save($communityAnswer)) {
                $this->Flash->success(__('The community answer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The community answer could not be saved. Please, try again.'));
        }
        $users = $this->CommunityAnswers->Users->find('list', ['limit' => 200]);
        $communities = $this->CommunityAnswers->Communities->find('list', ['limit' => 200]);
        $communityQuestions = $this->CommunityAnswers->CommunityQuestions->find('list', ['limit' => 200]);
        $parentCommunityAnswers = $this->CommunityAnswers->ParentCommunityAnswers->find('list', ['limit' => 200]);
        $this->set(compact('communityAnswer', 'users', 'communities', 'communityQuestions', 'parentCommunityAnswers'));
        $this->set('_serialize', ['communityAnswer']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Community Answer id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $communityAnswer = $this->CommunityAnswers->get($id);
        if ($this->CommunityAnswers->delete($communityAnswer)) {
            $this->Flash->success(__('The community answer has been deleted.'));
        } else {
            $this->Flash->error(__('The community answer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
