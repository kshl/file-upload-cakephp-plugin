<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CommunityCategories Controller
 *
 * @property \App\Model\Table\CommunityCategoriesTable $CommunityCategories
 */
class CommunityCategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentCommunityCategories']
        ];
        $communityCategories = $this->paginate($this->CommunityCategories);

        $this->set(compact('communityCategories'));
        $this->set('_serialize', ['communityCategories']);
    }

    /**
     * View method
     *
     * @param string|null $id Community Category id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $communityCategory = $this->CommunityCategories->get($id, [
            'contain' => ['ParentCommunityCategories', 'ChildCommunityCategories', 'CommunityCategoryMappings']
        ]);

        $this->set('communityCategory', $communityCategory);
        $this->set('_serialize', ['communityCategory']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $communityCategory = $this->CommunityCategories->newEntity();
        if ($this->request->is('post')) {
            $communityCategory = $this->CommunityCategories->patchEntity($communityCategory, $this->request->getData());
            if ($this->CommunityCategories->save($communityCategory)) {
                $this->Flash->success(__('The community category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The community category could not be saved. Please, try again.'));
        }
        $parentCommunityCategories = $this->CommunityCategories->ParentCommunityCategories->find('list', ['limit' => 200]);
        $this->set(compact('communityCategory', 'parentCommunityCategories'));
        $this->set('_serialize', ['communityCategory']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Community Category id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $communityCategory = $this->CommunityCategories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $communityCategory = $this->CommunityCategories->patchEntity($communityCategory, $this->request->getData());
            if ($this->CommunityCategories->save($communityCategory)) {
                $this->Flash->success(__('The community category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The community category could not be saved. Please, try again.'));
        }
        $parentCommunityCategories = $this->CommunityCategories->ParentCommunityCategories->find('list', ['limit' => 200]);
        $this->set(compact('communityCategory', 'parentCommunityCategories'));
        $this->set('_serialize', ['communityCategory']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Community Category id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $communityCategory = $this->CommunityCategories->get($id);
        if ($this->CommunityCategories->delete($communityCategory)) {
            $this->Flash->success(__('The community category has been deleted.'));
        } else {
            $this->Flash->error(__('The community category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
