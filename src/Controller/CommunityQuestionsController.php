<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CommunityQuestions Controller
 *
 * @property \App\Model\Table\CommunityQuestionsTable $CommunityQuestions
 */
class CommunityQuestionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Communities', 'Users']
        ];
        $communityQuestions = $this->paginate($this->CommunityQuestions);

        $this->set(compact('communityQuestions'));
        $this->set('_serialize', ['communityQuestions']);
    }

    /**
     * View method
     *
     * @param string|null $id Community Question id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $communityQuestion = $this->CommunityQuestions->get($id, [
            'contain' => ['Communities', 'Users', 'CommunityAnswers', 'CommunityQuestionFiles', 'CommunityQuestionKeywords', 'CommunityQuestionsLikes']
        ]);

        $this->set('communityQuestion', $communityQuestion);
        $this->set('_serialize', ['communityQuestion']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $communityQuestion = $this->CommunityQuestions->newEntity();
        if ($this->request->is('post')) {
            $communityQuestion = $this->CommunityQuestions->patchEntity($communityQuestion, $this->request->getData());
            if ($this->CommunityQuestions->save($communityQuestion)) {
                $this->Flash->success(__('The community question has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The community question could not be saved. Please, try again.'));
        }
        $communities = $this->CommunityQuestions->Communities->find('list', ['limit' => 200]);
        $users = $this->CommunityQuestions->Users->find('list', ['limit' => 200]);
        $this->set(compact('communityQuestion', 'communities', 'users'));
        $this->set('_serialize', ['communityQuestion']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Community Question id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $communityQuestion = $this->CommunityQuestions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $communityQuestion = $this->CommunityQuestions->patchEntity($communityQuestion, $this->request->getData());
            if ($this->CommunityQuestions->save($communityQuestion)) {
                $this->Flash->success(__('The community question has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The community question could not be saved. Please, try again.'));
        }
        $communities = $this->CommunityQuestions->Communities->find('list', ['limit' => 200]);
        $users = $this->CommunityQuestions->Users->find('list', ['limit' => 200]);
        $this->set(compact('communityQuestion', 'communities', 'users'));
        $this->set('_serialize', ['communityQuestion']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Community Question id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $communityQuestion = $this->CommunityQuestions->get($id);
        if ($this->CommunityQuestions->delete($communityQuestion)) {
            $this->Flash->success(__('The community question has been deleted.'));
        } else {
            $this->Flash->error(__('The community question could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
