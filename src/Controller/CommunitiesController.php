<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Communities Controller
 *
 * @property \App\Model\Table\CommunitiesTable $Communities
 */
class CommunitiesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('FileHandler');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'CommunityCategories']
        ];

        $communities = $this->paginate($this->Communities);

        $this->set(compact('communities'));
        $this->set('_serialize', ['communities']);
    }

    /**
     * View method
     *
     * @param string|null $id Community id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $community = $this->Communities->get($id, [
            'contain' => ['Users', 'CommunityCategories', 'CommunityAnswers', 'CommunityBlocks', 'CommunityInvites', 'CommunityQuestions', 'CommunityReports', 'CommunityReviews', 'CommunitySearchKeywords', 'CommunityUsers']
        ]);

        $this->set('community', $community);
        $this->set('logoUrl', $this->getLogoUrl($id));
        $this->set('_serialize', ['community']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $LogoFileAttrStatus = 'success';
        $bannerFileAttrStatus = 'success';

        $community = $this->Communities->newEntity();
        if ($this->request->is('post')) {
            
            $communityData = $this->request->getData();
            
            if (!empty($communityData['logo']['name'])) {
                $LogoFileAttr = $this->FileHandler->uploadImage($communityData['logo'], 'community/logo/56x56');

                if($LogoFileAttr['status'] == 'fail') {
                    $LogoFileAttrStatus = 'fail';
                    $this->Flash->error(__($LogoFileAttr['message']));
                } else {
                    $this->request->data['logo'] = $LogoFileAttr['file_name'];
                }
            }

            if (!empty($communityData['banner']['name'])) {
                $bannerFileAttr = $this->FileHandler->uploadImage($communityData['banner'], 'community/banner');

                if($bannerFileAttr['status'] == 'fail') {
                    $bannerFileAttrStatus = 'fail';
                    $this->Flash->error(__($bannerFileAttr['message']));
                } else {
                    $this->request->data['banner'] = $bannerFileAttr['file_name'];
                }
            }
            
            $community = $this->Communities->patchEntity($community, $this->request->data);

            if ($this->Communities->save($community) 
                    && $LogoFileAttrStatus == 'success' 
                    && $bannerFileAttrStatus == 'success') 
            {
                $this->Flash->success(__('The community has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The community could not be saved. Please, try again.'));
        }
        $users = $this->Communities->Users->find('list', ['limit' => 200]);
        $communityCategories = $this->Communities->CommunityCategories->find('list', ['limit' => 200]);
        $this->set(compact('community', 'users', 'communityCategories'));
        $this->set('_serialize', ['community']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Community id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $community = $this->Communities->get($id, [
            'contain' => ['CommunityCategories']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $community = $this->Communities->patchEntity($community, $this->request->getData());
            if ($this->Communities->save($community)) {
                $this->Flash->success(__('The community has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The community could not be saved. Please, try again.'));
        }
        $users = $this->Communities->Users->find('list', ['limit' => 200]);
        
        $communityCategories = $this->Communities->CommunityCategories->find('list', ['limit' => 200]);
        
        $this->set(compact('community', 'users', 'communityCategories'));
        $this->set('_serialize', ['community']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Community id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $community = $this->Communities->get($id);
        if ($this->Communities->delete($community)) {
            $this->Flash->success(__('The community has been deleted.'));
        } else {
            $this->Flash->error(__('The community could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
