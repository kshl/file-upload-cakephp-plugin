<?php
/**
  * @var \App\View\AppView $this
  */
echo $this->element('Sidebar/default'); ?>
<div class="communities index large-10 medium-9 columns content">
    <h3><?= __('Communities') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <!-- <th scope="col"><?= $this->Paginator->sort('slug') ?></th> -->
                <th scope="col"><?= $this->Paginator->sort('logo') ?></th>
                <!-- <th scope="col"><?= $this->Paginator->sort('banner') ?></th> -->
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('category_id') ?></th>
                <!-- <th scope="col"><?= $this->Paginator->sort('rate') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th> -->
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($communities as $community): ?>
            <tr>
                <!-- <td><?= $this->Number->format($community->id) ?></td> -->
                <!-- <td><?= $this->Number->format($community->user_id) ?></td> -->
                <td><?= $this->Html->link(h($community->user->user_nicename),'/users/view/'.$community->user->ID) ?></td>
                <td><?= h($community->title) ?></td>
                <!-- <td><?= h($community->slug) ?></td> -->
                <!-- <td><?= h($community->logo) ?></td> -->
                <td><?php // echo $this->Html->image('community/logo/56x56/'.$community->logo, ['alt' => 'CakePHP']) ?>
                <?php echo $cell = $this->cell('CommunityLogo::display', [$community->id]); ?>
                </td>
                <!-- <td><?= h($community->banner) ?></td> -->
                <td><?= $this->Number->format($community->status) ?></td>
                <!-- <td><?= $this->Number->format($community->category_id) ?></td> -->
                <td><?= h($community->community_category->cat_name) ?></td>
                <!-- <td><?= $this->Number->format($community->rate) ?></td>
                <td><?= $this->Number->format($community->type) ?></td> -->
                <td><?= h($community->modified) ?></td>
                <td><?= h($community->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $community->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $community->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $community->id], ['confirm' => __('Are you sure you want to delete # {0}?', $community->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->element('Paginator/default'); ?>
</div>
