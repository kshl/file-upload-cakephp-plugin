<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $community->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $community->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Communities'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Community Answers'), ['controller' => 'CommunityAnswers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Answer'), ['controller' => 'CommunityAnswers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Blocks'), ['controller' => 'CommunityBlocks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Block'), ['controller' => 'CommunityBlocks', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Invites'), ['controller' => 'CommunityInvites', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Invite'), ['controller' => 'CommunityInvites', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Questions'), ['controller' => 'CommunityQuestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Question'), ['controller' => 'CommunityQuestions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Reports'), ['controller' => 'CommunityReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Report'), ['controller' => 'CommunityReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Reviews'), ['controller' => 'CommunityReviews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Review'), ['controller' => 'CommunityReviews', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Search Keywords'), ['controller' => 'CommunitySearchKeywords', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Search Keyword'), ['controller' => 'CommunitySearchKeywords', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Users'), ['controller' => 'CommunityUsers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community User'), ['controller' => 'CommunityUsers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="communities form large-9 medium-8 columns content">
    <?= $this->Form->create($community) ?>
    <fieldset>
        <legend><?= __('Edit Community') ?></legend>
        <?php
            echo $this->Form->control('user_id');
            echo $this->Form->control('title');
            echo $this->Form->control('slug');
            echo $this->Form->control('description');
            echo $this->Form->control('short_description');
            echo $this->Form->control('rules');
            echo $this->Form->control('logo');
            echo $this->Form->control('banner');
            echo $this->Form->control('status', ['min'=>0,'max'=>1]);
            echo $this->Form->control('category_id', ['options' => $communityCategories, 'empty' => 'choose one']);
            echo $this->Form->control('rate', ['readonly'=>true]);
            echo $this->Form->control('type', ['readonly'=>true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
