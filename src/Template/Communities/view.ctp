<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="communities view large-12 medium-12 columns content">
    <h3><?= h($community->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($community->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Slug') ?></th>
            <td><?= h($community->slug) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Logo') ?></th>
            <td><?php echo $cell = $this->cell('CommunityLogo::display', [$community->id]); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Banner') ?></th>
            <td><?= h($community->banner) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($community->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Id') ?></th>
            <td><?= $this->Number->format($community->user_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($community->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Category Id') ?></th>
            <!-- <td><?= $this->Number->format($community->category_id) ?></td> -->
            <td><?= h($community->community_category->cat_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rate') ?></th>
            <td><?= $this->Number->format($community->rate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= $this->Number->format($community->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($community->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($community->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($community->description)); ?>
    </div>
    <div class="row">
        <h4><?= __('Short Description') ?></h4>
        <?= $this->Text->autoParagraph(h($community->short_description)); ?>
    </div>
    <div class="row">
        <h4><?= __('Rules') ?></h4>
        <?= $this->Text->autoParagraph(h($community->rules)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Answers') ?></h4>
        <?php if (!empty($community->community_answers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('Community Question Id') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Answer') ?></th>
                <th scope="col"><?= __('Total Likes') ?></th>
                <th scope="col"><?= __('Total Dislikes') ?></th>
                <th scope="col"><?= __('Approved') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($community->community_answers as $communityAnswers): ?>
            <tr>
                <td><?= h($communityAnswers->id) ?></td>
                <td><?= h($communityAnswers->user_id) ?></td>
                <td><?= h($communityAnswers->community_id) ?></td>
                <td><?= h($communityAnswers->community_question_id) ?></td>
                <td><?= h($communityAnswers->parent_id) ?></td>
                <td><?= h($communityAnswers->answer) ?></td>
                <td><?= h($communityAnswers->total_likes) ?></td>
                <td><?= h($communityAnswers->total_dislikes) ?></td>
                <td><?= h($communityAnswers->approved) ?></td>
                <td><?= h($communityAnswers->modified) ?></td>
                <td><?= h($communityAnswers->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityAnswers', 'action' => 'view', $communityAnswers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityAnswers', 'action' => 'edit', $communityAnswers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityAnswers', 'action' => 'delete', $communityAnswers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityAnswers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Blocks') ?></h4>
        <?php if (!empty($community->community_blocks)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Reason') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($community->community_blocks as $communityBlocks): ?>
            <tr>
                <td><?= h($communityBlocks->id) ?></td>
                <td><?= h($communityBlocks->community_id) ?></td>
                <td><?= h($communityBlocks->user_id) ?></td>
                <td><?= h($communityBlocks->reason) ?></td>
                <td><?= h($communityBlocks->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityBlocks', 'action' => 'view', $communityBlocks->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityBlocks', 'action' => 'edit', $communityBlocks->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityBlocks', 'action' => 'delete', $communityBlocks->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityBlocks->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Invites') ?></h4>
        <?php if (!empty($community->community_invites)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Message') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($community->community_invites as $communityInvites): ?>
            <tr>
                <td><?= h($communityInvites->id) ?></td>
                <td><?= h($communityInvites->community_id) ?></td>
                <td><?= h($communityInvites->user_id) ?></td>
                <td><?= h($communityInvites->message) ?></td>
                <td><?= h($communityInvites->modified) ?></td>
                <td><?= h($communityInvites->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityInvites', 'action' => 'view', $communityInvites->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityInvites', 'action' => 'edit', $communityInvites->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityInvites', 'action' => 'delete', $communityInvites->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityInvites->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Questions') ?></h4>
        <?php if (!empty($community->community_questions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Total Comments') ?></th>
                <th scope="col"><?= __('Total Views') ?></th>
                <th scope="col"><?= __('Total Likes') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($community->community_questions as $communityQuestions): ?>
            <tr>
                <td><?= h($communityQuestions->id) ?></td>
                <td><?= h($communityQuestions->community_id) ?></td>
                <td><?= h($communityQuestions->user_id) ?></td>
                <td><?= h($communityQuestions->title) ?></td>
                <td><?= h($communityQuestions->description) ?></td>
                <td><?= h($communityQuestions->status) ?></td>
                <td><?= h($communityQuestions->total_comments) ?></td>
                <td><?= h($communityQuestions->total_views) ?></td>
                <td><?= h($communityQuestions->total_likes) ?></td>
                <td><?= h($communityQuestions->modified) ?></td>
                <td><?= h($communityQuestions->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityQuestions', 'action' => 'view', $communityQuestions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityQuestions', 'action' => 'edit', $communityQuestions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityQuestions', 'action' => 'delete', $communityQuestions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityQuestions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Reports') ?></h4>
        <?php if (!empty($community->community_reports)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Object Id') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($community->community_reports as $communityReports): ?>
            <tr>
                <td><?= h($communityReports->id) ?></td>
                <td><?= h($communityReports->community_id) ?></td>
                <td><?= h($communityReports->user_id) ?></td>
                <td><?= h($communityReports->object_id) ?></td>
                <td><?= h($communityReports->type) ?></td>
                <td><?= h($communityReports->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityReports', 'action' => 'view', $communityReports->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityReports', 'action' => 'edit', $communityReports->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityReports', 'action' => 'delete', $communityReports->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityReports->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Reviews') ?></h4>
        <?php if (!empty($community->community_reviews)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('Rating') ?></th>
                <th scope="col"><?= __('Reviews') ?></th>
                <th scope="col"><?= __('Helpful') ?></th>
                <th scope="col"><?= __('Reason') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($community->community_reviews as $communityReviews): ?>
            <tr>
                <td><?= h($communityReviews->id) ?></td>
                <td><?= h($communityReviews->user_id) ?></td>
                <td><?= h($communityReviews->community_id) ?></td>
                <td><?= h($communityReviews->rating) ?></td>
                <td><?= h($communityReviews->reviews) ?></td>
                <td><?= h($communityReviews->helpful) ?></td>
                <td><?= h($communityReviews->reason) ?></td>
                <td><?= h($communityReviews->modified) ?></td>
                <td><?= h($communityReviews->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityReviews', 'action' => 'view', $communityReviews->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityReviews', 'action' => 'edit', $communityReviews->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityReviews', 'action' => 'delete', $communityReviews->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityReviews->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Search Keywords') ?></h4>
        <?php if (!empty($community->community_search_keywords)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('Search Keyword Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($community->community_search_keywords as $communitySearchKeywords): ?>
            <tr>
                <td><?= h($communitySearchKeywords->id) ?></td>
                <td><?= h($communitySearchKeywords->community_id) ?></td>
                <td><?= h($communitySearchKeywords->search_keyword_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunitySearchKeywords', 'action' => 'view', $communitySearchKeywords->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunitySearchKeywords', 'action' => 'edit', $communitySearchKeywords->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunitySearchKeywords', 'action' => 'delete', $communitySearchKeywords->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communitySearchKeywords->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Users') ?></h4>
        <?php if (!empty($community->community_users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('User Type') ?></th>
                <th scope="col"><?= __('Approved') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($community->community_users as $communityUsers): ?>
            <tr>
                <td><?= h($communityUsers->id) ?></td>
                <td><?= h($communityUsers->community_id) ?></td>
                <td><?= h($communityUsers->user_id) ?></td>
                <td><?= h($communityUsers->user_type) ?></td>
                <td><?= h($communityUsers->approved) ?></td>
                <td><?= h($communityUsers->modified) ?></td>
                <td><?= h($communityUsers->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityUsers', 'action' => 'view', $communityUsers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityUsers', 'action' => 'edit', $communityUsers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityUsers', 'action' => 'delete', $communityUsers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityUsers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
