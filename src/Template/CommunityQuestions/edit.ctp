<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $communityQuestion->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $communityQuestion->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Community Questions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Communities'), ['controller' => 'Communities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community'), ['controller' => 'Communities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Answers'), ['controller' => 'CommunityAnswers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Answer'), ['controller' => 'CommunityAnswers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Question Files'), ['controller' => 'CommunityQuestionFiles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Question File'), ['controller' => 'CommunityQuestionFiles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Question Keywords'), ['controller' => 'CommunityQuestionKeywords', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Question Keyword'), ['controller' => 'CommunityQuestionKeywords', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Questions Likes'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Questions Like'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="communityQuestions form large-9 medium-8 columns content">
    <?= $this->Form->create($communityQuestion) ?>
    <fieldset>
        <legend><?= __('Edit Community Question') ?></legend>
        <?php
            echo $this->Form->control('community_id', ['options' => $communities]);
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('title');
            echo $this->Form->control('description');
            echo $this->Form->control('status', ['max'=>1, 'min'=>0]);
            echo $this->Form->control('total_comments');
            echo $this->Form->control('total_views');
            echo $this->Form->control('total_likes');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
