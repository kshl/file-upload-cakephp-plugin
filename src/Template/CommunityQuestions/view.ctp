<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Community Question'), ['action' => 'edit', $communityQuestion->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Community Question'), ['action' => 'delete', $communityQuestion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityQuestion->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Community Questions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Question'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Communities'), ['controller' => 'Communities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community'), ['controller' => 'Communities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Answers'), ['controller' => 'CommunityAnswers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Answer'), ['controller' => 'CommunityAnswers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Question Files'), ['controller' => 'CommunityQuestionFiles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Question File'), ['controller' => 'CommunityQuestionFiles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Question Keywords'), ['controller' => 'CommunityQuestionKeywords', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Question Keyword'), ['controller' => 'CommunityQuestionKeywords', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Questions Likes'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Questions Like'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="communityQuestions view large-9 medium-8 columns content">
    <h3><?= h($communityQuestion->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Community') ?></th>
            <td><?= $communityQuestion->has('community') ? $this->Html->link($communityQuestion->community->title, ['controller' => 'Communities', 'action' => 'view', $communityQuestion->community->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $communityQuestion->has('user') ? $this->Html->link($communityQuestion->user->ID, ['controller' => 'Users', 'action' => 'view', $communityQuestion->user->ID]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($communityQuestion->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($communityQuestion->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($communityQuestion->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Comments') ?></th>
            <td><?= $this->Number->format($communityQuestion->total_comments) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Views') ?></th>
            <td><?= $this->Number->format($communityQuestion->total_views) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Likes') ?></th>
            <td><?= $this->Number->format($communityQuestion->total_likes) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($communityQuestion->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($communityQuestion->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($communityQuestion->description)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Answers') ?></h4>
        <?php if (!empty($communityQuestion->community_answers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('Community Question Id') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Answer') ?></th>
                <th scope="col"><?= __('Total Likes') ?></th>
                <th scope="col"><?= __('Total Dislikes') ?></th>
                <th scope="col"><?= __('Approved') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($communityQuestion->community_answers as $communityAnswers): ?>
            <tr>
                <td><?= h($communityAnswers->id) ?></td>
                <td><?= h($communityAnswers->user_id) ?></td>
                <td><?= h($communityAnswers->community_id) ?></td>
                <td><?= h($communityAnswers->community_question_id) ?></td>
                <td><?= h($communityAnswers->parent_id) ?></td>
                <td><?= h($communityAnswers->answer) ?></td>
                <td><?= h($communityAnswers->total_likes) ?></td>
                <td><?= h($communityAnswers->total_dislikes) ?></td>
                <td><?= h($communityAnswers->approved) ?></td>
                <td><?= h($communityAnswers->modified) ?></td>
                <td><?= h($communityAnswers->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityAnswers', 'action' => 'view', $communityAnswers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityAnswers', 'action' => 'edit', $communityAnswers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityAnswers', 'action' => 'delete', $communityAnswers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityAnswers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Question Files') ?></h4>
        <?php if (!empty($communityQuestion->community_question_files)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Question Id') ?></th>
                <th scope="col"><?= __('File Name') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($communityQuestion->community_question_files as $communityQuestionFiles): ?>
            <tr>
                <td><?= h($communityQuestionFiles->id) ?></td>
                <td><?= h($communityQuestionFiles->community_question_id) ?></td>
                <td><?= h($communityQuestionFiles->file_name) ?></td>
                <td><?= h($communityQuestionFiles->type) ?></td>
                <td><?= h($communityQuestionFiles->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityQuestionFiles', 'action' => 'view', $communityQuestionFiles->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityQuestionFiles', 'action' => 'edit', $communityQuestionFiles->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityQuestionFiles', 'action' => 'delete', $communityQuestionFiles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityQuestionFiles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Question Keywords') ?></h4>
        <?php if (!empty($communityQuestion->community_question_keywords)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Question Id') ?></th>
                <th scope="col"><?= __('Search Keyword Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($communityQuestion->community_question_keywords as $communityQuestionKeywords): ?>
            <tr>
                <td><?= h($communityQuestionKeywords->id) ?></td>
                <td><?= h($communityQuestionKeywords->community_question_id) ?></td>
                <td><?= h($communityQuestionKeywords->search_keyword_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityQuestionKeywords', 'action' => 'view', $communityQuestionKeywords->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityQuestionKeywords', 'action' => 'edit', $communityQuestionKeywords->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityQuestionKeywords', 'action' => 'delete', $communityQuestionKeywords->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityQuestionKeywords->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Questions Likes') ?></h4>
        <?php if (!empty($communityQuestion->community_questions_likes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Question Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Like Dislike') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($communityQuestion->community_questions_likes as $communityQuestionsLikes): ?>
            <tr>
                <td><?= h($communityQuestionsLikes->id) ?></td>
                <td><?= h($communityQuestionsLikes->community_question_id) ?></td>
                <td><?= h($communityQuestionsLikes->user_id) ?></td>
                <td><?= h($communityQuestionsLikes->like_dislike) ?></td>
                <td><?= h($communityQuestionsLikes->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'view', $communityQuestionsLikes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'edit', $communityQuestionsLikes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'delete', $communityQuestionsLikes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityQuestionsLikes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
