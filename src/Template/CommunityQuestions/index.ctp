<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Community Question'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Communities'), ['controller' => 'Communities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community'), ['controller' => 'Communities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Answers'), ['controller' => 'CommunityAnswers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Answer'), ['controller' => 'CommunityAnswers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Question Files'), ['controller' => 'CommunityQuestionFiles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Question File'), ['controller' => 'CommunityQuestionFiles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Question Keywords'), ['controller' => 'CommunityQuestionKeywords', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Question Keyword'), ['controller' => 'CommunityQuestionKeywords', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Questions Likes'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Questions Like'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="communityQuestions index large-9 medium-8 columns content">
    <h3><?= __('Community Questions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('community_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total_comments') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total_views') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total_likes') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($communityQuestions as $communityQuestion): ?>
            <tr>
                <td><?= $this->Number->format($communityQuestion->id) ?></td>
                <td><?= $communityQuestion->has('community') ? $this->Html->link($communityQuestion->community->title, ['controller' => 'Communities', 'action' => 'view', $communityQuestion->community->id]) : '' ?></td>
                <td><?= $communityQuestion->has('user') ? $this->Html->link($communityQuestion->user->ID, ['controller' => 'Users', 'action' => 'view', $communityQuestion->user->ID]) : '' ?></td>
                <td><?= h($communityQuestion->title) ?></td>
                <td><?= $this->Number->format($communityQuestion->status) ?></td>
                <td><?= $this->Number->format($communityQuestion->total_comments) ?></td>
                <td><?= $this->Number->format($communityQuestion->total_views) ?></td>
                <td><?= $this->Number->format($communityQuestion->total_likes) ?></td>
                <td><?= h($communityQuestion->modified) ?></td>
                <td><?= h($communityQuestion->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $communityQuestion->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $communityQuestion->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $communityQuestion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityQuestion->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
