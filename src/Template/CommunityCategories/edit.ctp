<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $communityCategory->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $communityCategory->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Community Categories'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Parent Community Categories'), ['controller' => 'CommunityCategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Parent Community Category'), ['controller' => 'CommunityCategories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Category Mappings'), ['controller' => 'CommunityCategoryMappings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Category Mapping'), ['controller' => 'CommunityCategoryMappings', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="communityCategories form large-9 medium-8 columns content">
    <?= $this->Form->create($communityCategory) ?>
    <fieldset>
        <legend><?= __('Edit Community Category') ?></legend>
        <?php
            echo $this->Form->control('parent_id', ['options' => $parentCommunityCategories]);
            echo $this->Form->control('title');
            echo $this->Form->control('cat_name');
            echo $this->Form->control('short_description');
            echo $this->Form->control('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
