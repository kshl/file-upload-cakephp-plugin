<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Community Category'), ['action' => 'edit', $communityCategory->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Community Category'), ['action' => 'delete', $communityCategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityCategory->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Community Categories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Category'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Parent Community Categories'), ['controller' => 'CommunityCategories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Parent Community Category'), ['controller' => 'CommunityCategories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Category Mappings'), ['controller' => 'CommunityCategoryMappings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Category Mapping'), ['controller' => 'CommunityCategoryMappings', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="communityCategories view large-9 medium-8 columns content">
    <h3><?= h($communityCategory->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Parent Community Category') ?></th>
            <td><?= $communityCategory->has('parent_community_category') ? $this->Html->link($communityCategory->parent_community_category->title, ['controller' => 'CommunityCategories', 'action' => 'view', $communityCategory->parent_community_category->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($communityCategory->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cat Name') ?></th>
            <td><?= h($communityCategory->cat_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Short Description') ?></th>
            <td><?= h($communityCategory->short_description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($communityCategory->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($communityCategory->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($communityCategory->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($communityCategory->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Community Categories') ?></h4>
        <?php if (!empty($communityCategory->child_community_categories)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Cat Name') ?></th>
                <th scope="col"><?= __('Short Description') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($communityCategory->child_community_categories as $childCommunityCategories): ?>
            <tr>
                <td><?= h($childCommunityCategories->id) ?></td>
                <td><?= h($childCommunityCategories->parent_id) ?></td>
                <td><?= h($childCommunityCategories->title) ?></td>
                <td><?= h($childCommunityCategories->cat_name) ?></td>
                <td><?= h($childCommunityCategories->short_description) ?></td>
                <td><?= h($childCommunityCategories->status) ?></td>
                <td><?= h($childCommunityCategories->created) ?></td>
                <td><?= h($childCommunityCategories->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityCategories', 'action' => 'view', $childCommunityCategories->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityCategories', 'action' => 'edit', $childCommunityCategories->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityCategories', 'action' => 'delete', $childCommunityCategories->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childCommunityCategories->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Category Mappings') ?></h4>
        <?php if (!empty($communityCategory->community_category_mappings)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Category Id') ?></th>
                <th scope="col"><?= __('Cat Terms Id') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($communityCategory->community_category_mappings as $communityCategoryMappings): ?>
            <tr>
                <td><?= h($communityCategoryMappings->id) ?></td>
                <td><?= h($communityCategoryMappings->community_category_id) ?></td>
                <td><?= h($communityCategoryMappings->cat_terms_id) ?></td>
                <td><?= h($communityCategoryMappings->type) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityCategoryMappings', 'action' => 'view', $communityCategoryMappings->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityCategoryMappings', 'action' => 'edit', $communityCategoryMappings->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityCategoryMappings', 'action' => 'delete', $communityCategoryMappings->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityCategoryMappings->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
