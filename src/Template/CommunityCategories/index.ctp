<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Community Category'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Category Mappings'), ['controller' => 'CommunityCategoryMappings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Category Mapping'), ['controller' => 'CommunityCategoryMappings', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="communityCategories index large-9 medium-8 columns content">
    <h3><?= __('Community Categories') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cat_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('short_description') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($communityCategories as $communityCategory): ?>
            <tr>
                <td><?= $this->Number->format($communityCategory->id) ?></td>
                <td><?= $communityCategory->has('parent_community_category') ? $this->Html->link($communityCategory->parent_community_category->title, ['controller' => 'CommunityCategories', 'action' => 'view', $communityCategory->parent_community_category->id]) : '' ?></td>
                <td><?= h($communityCategory->title) ?></td>
                <td><?= h($communityCategory->cat_name) ?></td>
                <td><?= h($communityCategory->short_description) ?></td>
                <td><?= $this->Number->format($communityCategory->status) ?></td>
                <td><?= h($communityCategory->created) ?></td>
                <td><?= h($communityCategory->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $communityCategory->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $communityCategory->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $communityCategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityCategory->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
