<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Communities'), ['controller' => 'Communities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community'), ['controller' => 'Communities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Answer Approvals'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Answer Approval'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Answer Likes'), ['controller' => 'CommunityAnswerLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Answer Like'), ['controller' => 'CommunityAnswerLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Answers'), ['controller' => 'CommunityAnswers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Answer'), ['controller' => 'CommunityAnswers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Blocks'), ['controller' => 'CommunityBlocks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Block'), ['controller' => 'CommunityBlocks', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Invites'), ['controller' => 'CommunityInvites', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Invite'), ['controller' => 'CommunityInvites', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Questions'), ['controller' => 'CommunityQuestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Question'), ['controller' => 'CommunityQuestions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Questions Likes'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Questions Like'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Reports'), ['controller' => 'CommunityReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Report'), ['controller' => 'CommunityReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Reviews'), ['controller' => 'CommunityReviews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Review'), ['controller' => 'CommunityReviews', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Users'), ['controller' => 'CommunityUsers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community User'), ['controller' => 'CommunityUsers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <?php
            echo $this->Form->control('user_login');
            echo $this->Form->control('user_pass');
            echo $this->Form->control('user_nicename');
            echo $this->Form->control('user_email');
            echo $this->Form->control('user_url');
            echo $this->Form->control('user_registered');
            echo $this->Form->control('user_activation_key');
            echo $this->Form->control('user_status');
            echo $this->Form->control('display_name');
            echo $this->Form->control('sub_domain');
            echo $this->Form->control('subdomain_status');
            echo $this->Form->control('reg_type');
            echo $this->Form->control('mobile');
            echo $this->Form->control('add1');
            echo $this->Form->control('add2');
            echo $this->Form->control('dob');
            echo $this->Form->control('about');
            echo $this->Form->control('profession');
            echo $this->Form->control('user_img');
            echo $this->Form->control('father_name');
            echo $this->Form->control('pin');
            echo $this->Form->control('last_name');
            echo $this->Form->control('country');
            echo $this->Form->control('state');
            echo $this->Form->control('city');
            echo $this->Form->control('recover_pass');
            echo $this->Form->control('month_year');
            echo $this->Form->control('reg_loc');
            echo $this->Form->control('ip_address');
            echo $this->Form->control('modifieddate', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
