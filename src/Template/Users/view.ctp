<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->ID]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $user->ID)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Communities'), ['controller' => 'Communities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community'), ['controller' => 'Communities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Answer Approvals'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Answer Approval'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Answer Likes'), ['controller' => 'CommunityAnswerLikes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Answer Like'), ['controller' => 'CommunityAnswerLikes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Answers'), ['controller' => 'CommunityAnswers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Answer'), ['controller' => 'CommunityAnswers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Blocks'), ['controller' => 'CommunityBlocks', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Block'), ['controller' => 'CommunityBlocks', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Invites'), ['controller' => 'CommunityInvites', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Invite'), ['controller' => 'CommunityInvites', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Questions'), ['controller' => 'CommunityQuestions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Question'), ['controller' => 'CommunityQuestions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Questions Likes'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Questions Like'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Reports'), ['controller' => 'CommunityReports', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Report'), ['controller' => 'CommunityReports', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Reviews'), ['controller' => 'CommunityReviews', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Review'), ['controller' => 'CommunityReviews', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Users'), ['controller' => 'CommunityUsers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community User'), ['controller' => 'CommunityUsers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->ID) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User Login') ?></th>
            <td><?= h($user->user_login) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Pass') ?></th>
            <td><?= h($user->user_pass) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Nicename') ?></th>
            <td><?= h($user->user_nicename) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Email') ?></th>
            <td><?= h($user->user_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Url') ?></th>
            <td><?= h($user->user_url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Activation Key') ?></th>
            <td><?= h($user->user_activation_key) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Display Name') ?></th>
            <td><?= h($user->display_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sub Domain') ?></th>
            <td><?= h($user->sub_domain) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Add1') ?></th>
            <td><?= h($user->add1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Add2') ?></th>
            <td><?= h($user->add2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('About') ?></th>
            <td><?= h($user->about) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Profession') ?></th>
            <td><?= h($user->profession) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Img') ?></th>
            <td><?= h($user->user_img) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Father Name') ?></th>
            <td><?= h($user->father_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($user->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Recover Pass') ?></th>
            <td><?= h($user->recover_pass) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Month Year') ?></th>
            <td><?= h($user->month_year) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Reg Loc') ?></th>
            <td><?= h($user->reg_loc) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ip Address') ?></th>
            <td><?= h($user->ip_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($user->ID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Status') ?></th>
            <td><?= $this->Number->format($user->user_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Reg Type') ?></th>
            <td><?= $this->Number->format($user->reg_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mobile') ?></th>
            <td><?= $this->Number->format($user->mobile) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pin') ?></th>
            <td><?= $this->Number->format($user->pin) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Country') ?></th>
            <td><?= $this->Number->format($user->country) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('State') ?></th>
            <td><?= $this->Number->format($user->state) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('City') ?></th>
            <td><?= $this->Number->format($user->city) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Registered') ?></th>
            <td><?= h($user->user_registered) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dob') ?></th>
            <td><?= h($user->dob) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modifieddate') ?></th>
            <td><?= h($user->modifieddate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subdomain Status') ?></th>
            <td><?= $user->subdomain_status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Communities') ?></h4>
        <?php if (!empty($user->communities)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Short Description') ?></th>
                <th scope="col"><?= __('Rules') ?></th>
                <th scope="col"><?= __('Logo') ?></th>
                <th scope="col"><?= __('Banner') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Category Id') ?></th>
                <th scope="col"><?= __('Rate') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->communities as $communities): ?>
            <tr>
                <td><?= h($communities->id) ?></td>
                <td><?= h($communities->user_id) ?></td>
                <td><?= h($communities->title) ?></td>
                <td><?= h($communities->slug) ?></td>
                <td><?= h($communities->description) ?></td>
                <td><?= h($communities->short_description) ?></td>
                <td><?= h($communities->rules) ?></td>
                <td><?= h($communities->logo) ?></td>
                <td><?= h($communities->banner) ?></td>
                <td><?= h($communities->status) ?></td>
                <td><?= h($communities->category_id) ?></td>
                <td><?= h($communities->rate) ?></td>
                <td><?= h($communities->type) ?></td>
                <td><?= h($communities->modified) ?></td>
                <td><?= h($communities->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Communities', 'action' => 'view', $communities->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Communities', 'action' => 'edit', $communities->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Communities', 'action' => 'delete', $communities->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communities->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Answer Approvals') ?></h4>
        <?php if (!empty($user->community_answer_approvals)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Answer Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->community_answer_approvals as $communityAnswerApprovals): ?>
            <tr>
                <td><?= h($communityAnswerApprovals->id) ?></td>
                <td><?= h($communityAnswerApprovals->community_answer_id) ?></td>
                <td><?= h($communityAnswerApprovals->user_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'view', $communityAnswerApprovals->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'edit', $communityAnswerApprovals->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'delete', $communityAnswerApprovals->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityAnswerApprovals->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Answer Likes') ?></h4>
        <?php if (!empty($user->community_answer_likes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Answer Id') ?></th>
                <th scope="col"><?= __('Like Dislike') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->community_answer_likes as $communityAnswerLikes): ?>
            <tr>
                <td><?= h($communityAnswerLikes->id) ?></td>
                <td><?= h($communityAnswerLikes->community_answer_id) ?></td>
                <td><?= h($communityAnswerLikes->like_dislike) ?></td>
                <td><?= h($communityAnswerLikes->user_id) ?></td>
                <td><?= h($communityAnswerLikes->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityAnswerLikes', 'action' => 'view', $communityAnswerLikes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityAnswerLikes', 'action' => 'edit', $communityAnswerLikes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityAnswerLikes', 'action' => 'delete', $communityAnswerLikes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityAnswerLikes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Answers') ?></h4>
        <?php if (!empty($user->community_answers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('Community Question Id') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Answer') ?></th>
                <th scope="col"><?= __('Total Likes') ?></th>
                <th scope="col"><?= __('Total Dislikes') ?></th>
                <th scope="col"><?= __('Approved') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->community_answers as $communityAnswers): ?>
            <tr>
                <td><?= h($communityAnswers->id) ?></td>
                <td><?= h($communityAnswers->user_id) ?></td>
                <td><?= h($communityAnswers->community_id) ?></td>
                <td><?= h($communityAnswers->community_question_id) ?></td>
                <td><?= h($communityAnswers->parent_id) ?></td>
                <td><?= h($communityAnswers->answer) ?></td>
                <td><?= h($communityAnswers->total_likes) ?></td>
                <td><?= h($communityAnswers->total_dislikes) ?></td>
                <td><?= h($communityAnswers->approved) ?></td>
                <td><?= h($communityAnswers->modified) ?></td>
                <td><?= h($communityAnswers->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityAnswers', 'action' => 'view', $communityAnswers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityAnswers', 'action' => 'edit', $communityAnswers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityAnswers', 'action' => 'delete', $communityAnswers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityAnswers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Blocks') ?></h4>
        <?php if (!empty($user->community_blocks)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Reason') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->community_blocks as $communityBlocks): ?>
            <tr>
                <td><?= h($communityBlocks->id) ?></td>
                <td><?= h($communityBlocks->community_id) ?></td>
                <td><?= h($communityBlocks->user_id) ?></td>
                <td><?= h($communityBlocks->reason) ?></td>
                <td><?= h($communityBlocks->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityBlocks', 'action' => 'view', $communityBlocks->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityBlocks', 'action' => 'edit', $communityBlocks->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityBlocks', 'action' => 'delete', $communityBlocks->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityBlocks->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Invites') ?></h4>
        <?php if (!empty($user->community_invites)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Message') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->community_invites as $communityInvites): ?>
            <tr>
                <td><?= h($communityInvites->id) ?></td>
                <td><?= h($communityInvites->community_id) ?></td>
                <td><?= h($communityInvites->user_id) ?></td>
                <td><?= h($communityInvites->message) ?></td>
                <td><?= h($communityInvites->modified) ?></td>
                <td><?= h($communityInvites->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityInvites', 'action' => 'view', $communityInvites->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityInvites', 'action' => 'edit', $communityInvites->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityInvites', 'action' => 'delete', $communityInvites->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityInvites->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Questions') ?></h4>
        <?php if (!empty($user->community_questions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Total Comments') ?></th>
                <th scope="col"><?= __('Total Views') ?></th>
                <th scope="col"><?= __('Total Likes') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->community_questions as $communityQuestions): ?>
            <tr>
                <td><?= h($communityQuestions->id) ?></td>
                <td><?= h($communityQuestions->community_id) ?></td>
                <td><?= h($communityQuestions->user_id) ?></td>
                <td><?= h($communityQuestions->title) ?></td>
                <td><?= h($communityQuestions->description) ?></td>
                <td><?= h($communityQuestions->status) ?></td>
                <td><?= h($communityQuestions->total_comments) ?></td>
                <td><?= h($communityQuestions->total_views) ?></td>
                <td><?= h($communityQuestions->total_likes) ?></td>
                <td><?= h($communityQuestions->modified) ?></td>
                <td><?= h($communityQuestions->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityQuestions', 'action' => 'view', $communityQuestions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityQuestions', 'action' => 'edit', $communityQuestions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityQuestions', 'action' => 'delete', $communityQuestions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityQuestions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Questions Likes') ?></h4>
        <?php if (!empty($user->community_questions_likes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Question Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Like Dislike') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->community_questions_likes as $communityQuestionsLikes): ?>
            <tr>
                <td><?= h($communityQuestionsLikes->id) ?></td>
                <td><?= h($communityQuestionsLikes->community_question_id) ?></td>
                <td><?= h($communityQuestionsLikes->user_id) ?></td>
                <td><?= h($communityQuestionsLikes->like_dislike) ?></td>
                <td><?= h($communityQuestionsLikes->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'view', $communityQuestionsLikes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'edit', $communityQuestionsLikes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'delete', $communityQuestionsLikes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityQuestionsLikes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Reports') ?></h4>
        <?php if (!empty($user->community_reports)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Object Id') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->community_reports as $communityReports): ?>
            <tr>
                <td><?= h($communityReports->id) ?></td>
                <td><?= h($communityReports->community_id) ?></td>
                <td><?= h($communityReports->user_id) ?></td>
                <td><?= h($communityReports->object_id) ?></td>
                <td><?= h($communityReports->type) ?></td>
                <td><?= h($communityReports->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityReports', 'action' => 'view', $communityReports->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityReports', 'action' => 'edit', $communityReports->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityReports', 'action' => 'delete', $communityReports->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityReports->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Reviews') ?></h4>
        <?php if (!empty($user->community_reviews)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('Rating') ?></th>
                <th scope="col"><?= __('Reviews') ?></th>
                <th scope="col"><?= __('Helpful') ?></th>
                <th scope="col"><?= __('Reason') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->community_reviews as $communityReviews): ?>
            <tr>
                <td><?= h($communityReviews->id) ?></td>
                <td><?= h($communityReviews->user_id) ?></td>
                <td><?= h($communityReviews->community_id) ?></td>
                <td><?= h($communityReviews->rating) ?></td>
                <td><?= h($communityReviews->reviews) ?></td>
                <td><?= h($communityReviews->helpful) ?></td>
                <td><?= h($communityReviews->reason) ?></td>
                <td><?= h($communityReviews->modified) ?></td>
                <td><?= h($communityReviews->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityReviews', 'action' => 'view', $communityReviews->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityReviews', 'action' => 'edit', $communityReviews->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityReviews', 'action' => 'delete', $communityReviews->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityReviews->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Users') ?></h4>
        <?php if (!empty($user->community_users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('User Type') ?></th>
                <th scope="col"><?= __('Approved') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->community_users as $communityUsers): ?>
            <tr>
                <td><?= h($communityUsers->id) ?></td>
                <td><?= h($communityUsers->community_id) ?></td>
                <td><?= h($communityUsers->user_id) ?></td>
                <td><?= h($communityUsers->user_type) ?></td>
                <td><?= h($communityUsers->approved) ?></td>
                <td><?= h($communityUsers->modified) ?></td>
                <td><?= h($communityUsers->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityUsers', 'action' => 'view', $communityUsers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityUsers', 'action' => 'edit', $communityUsers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityUsers', 'action' => 'delete', $communityUsers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityUsers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
