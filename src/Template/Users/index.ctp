<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Communities'), ['controller' => 'Communities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community'), ['controller' => 'Communities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Answer Approvals'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Answer Approval'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Answer Likes'), ['controller' => 'CommunityAnswerLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Answer Like'), ['controller' => 'CommunityAnswerLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Answers'), ['controller' => 'CommunityAnswers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Answer'), ['controller' => 'CommunityAnswers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Blocks'), ['controller' => 'CommunityBlocks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Block'), ['controller' => 'CommunityBlocks', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Invites'), ['controller' => 'CommunityInvites', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Invite'), ['controller' => 'CommunityInvites', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Questions'), ['controller' => 'CommunityQuestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Question'), ['controller' => 'CommunityQuestions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Questions Likes'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Questions Like'), ['controller' => 'CommunityQuestionsLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Reports'), ['controller' => 'CommunityReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Report'), ['controller' => 'CommunityReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Reviews'), ['controller' => 'CommunityReviews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Review'), ['controller' => 'CommunityReviews', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Users'), ['controller' => 'CommunityUsers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community User'), ['controller' => 'CommunityUsers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Users') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('user_login') ?></th>
                <th scope="col"><?= $this->Paginator->sort('display_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mobile') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dob') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_img') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modifieddate') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                
                <td><?= h($user->user_login) ?></td>
                <td><?= h($user->display_name .' '. $user->last_name) ?></td>
                <td><?= h($user->mobile) ?></td>
                <td><?= h($user->dob) ?></td>
                <td><?= h($user->user_img) ?></td>
                <td><?= h($user->modifieddate) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $user->ID]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->ID]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $user->ID)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
