<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Community Answer'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Communities'), ['controller' => 'Communities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community'), ['controller' => 'Communities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Questions'), ['controller' => 'CommunityQuestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Question'), ['controller' => 'CommunityQuestions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Answer Approvals'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Answer Approval'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Answer Likes'), ['controller' => 'CommunityAnswerLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Answer Like'), ['controller' => 'CommunityAnswerLikes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="communityAnswers index large-9 medium-8 columns content">
    <h3><?= __('Community Answers') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('community_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('community_question_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total_likes') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total_dislikes') ?></th>
                <th scope="col"><?= $this->Paginator->sort('approved') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($communityAnswers as $communityAnswer): ?>
            <tr>
                <td><?= $this->Number->format($communityAnswer->id) ?></td>
                <td><?= $communityAnswer->has('user') ? $this->Html->link($communityAnswer->user->ID, ['controller' => 'Users', 'action' => 'view', $communityAnswer->user->ID]) : '' ?></td>
                <td><?= $communityAnswer->has('community') ? $this->Html->link($communityAnswer->community->title, ['controller' => 'Communities', 'action' => 'view', $communityAnswer->community->id]) : '' ?></td>
                <td><?= $communityAnswer->has('community_question') ? $this->Html->link($communityAnswer->community_question->title, ['controller' => 'CommunityQuestions', 'action' => 'view', $communityAnswer->community_question->id]) : '' ?></td>
                <td><?= $communityAnswer->has('parent_community_answer') ? $this->Html->link($communityAnswer->parent_community_answer->id, ['controller' => 'CommunityAnswers', 'action' => 'view', $communityAnswer->parent_community_answer->id]) : '' ?></td>
                <td><?= $this->Number->format($communityAnswer->total_likes) ?></td>
                <td><?= $this->Number->format($communityAnswer->total_dislikes) ?></td>
                <td><?= $this->Number->format($communityAnswer->approved) ?></td>
                <td><?= h($communityAnswer->modified) ?></td>
                <td><?= h($communityAnswer->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $communityAnswer->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $communityAnswer->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $communityAnswer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityAnswer->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
