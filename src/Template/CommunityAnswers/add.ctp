<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Community Answers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Communities'), ['controller' => 'Communities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community'), ['controller' => 'Communities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Questions'), ['controller' => 'CommunityQuestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Question'), ['controller' => 'CommunityQuestions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Parent Community Answers'), ['controller' => 'CommunityAnswers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Parent Community Answer'), ['controller' => 'CommunityAnswers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Answer Approvals'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Answer Approval'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Community Answer Likes'), ['controller' => 'CommunityAnswerLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Community Answer Like'), ['controller' => 'CommunityAnswerLikes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="communityAnswers form large-9 medium-8 columns content">
    <?= $this->Form->create($communityAnswer) ?>
    <fieldset>
        <legend><?= __('Add Community Answer') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('community_id', ['options' => $communities]);
            echo $this->Form->control('community_question_id', ['options' => $communityQuestions]);
            echo $this->Form->control('parent_id', ['options' => $parentCommunityAnswers]);
            echo $this->Form->control('answer');
            echo $this->Form->control('total_likes');
            echo $this->Form->control('total_dislikes');
            echo $this->Form->control('approved');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
