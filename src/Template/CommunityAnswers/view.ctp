<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Community Answer'), ['action' => 'edit', $communityAnswer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Community Answer'), ['action' => 'delete', $communityAnswer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityAnswer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Community Answers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Answer'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Communities'), ['controller' => 'Communities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community'), ['controller' => 'Communities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Questions'), ['controller' => 'CommunityQuestions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Question'), ['controller' => 'CommunityQuestions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Parent Community Answers'), ['controller' => 'CommunityAnswers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Parent Community Answer'), ['controller' => 'CommunityAnswers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Answer Approvals'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Answer Approval'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Community Answer Likes'), ['controller' => 'CommunityAnswerLikes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Community Answer Like'), ['controller' => 'CommunityAnswerLikes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="communityAnswers view large-9 medium-8 columns content">
    <h3><?= h($communityAnswer->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $communityAnswer->has('user') ? $this->Html->link($communityAnswer->user->ID, ['controller' => 'Users', 'action' => 'view', $communityAnswer->user->ID]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Community') ?></th>
            <td><?= $communityAnswer->has('community') ? $this->Html->link($communityAnswer->community->title, ['controller' => 'Communities', 'action' => 'view', $communityAnswer->community->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Community Question') ?></th>
            <td><?= $communityAnswer->has('community_question') ? $this->Html->link($communityAnswer->community_question->title, ['controller' => 'CommunityQuestions', 'action' => 'view', $communityAnswer->community_question->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Parent Community Answer') ?></th>
            <td><?= $communityAnswer->has('parent_community_answer') ? $this->Html->link($communityAnswer->parent_community_answer->id, ['controller' => 'CommunityAnswers', 'action' => 'view', $communityAnswer->parent_community_answer->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($communityAnswer->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Likes') ?></th>
            <td><?= $this->Number->format($communityAnswer->total_likes) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Dislikes') ?></th>
            <td><?= $this->Number->format($communityAnswer->total_dislikes) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Approved') ?></th>
            <td><?= $this->Number->format($communityAnswer->approved) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($communityAnswer->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($communityAnswer->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Answer') ?></h4>
        <?= $this->Text->autoParagraph(h($communityAnswer->answer)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Answer Approvals') ?></h4>
        <?php if (!empty($communityAnswer->community_answer_approvals)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Answer Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($communityAnswer->community_answer_approvals as $communityAnswerApprovals): ?>
            <tr>
                <td><?= h($communityAnswerApprovals->id) ?></td>
                <td><?= h($communityAnswerApprovals->community_answer_id) ?></td>
                <td><?= h($communityAnswerApprovals->user_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'view', $communityAnswerApprovals->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'edit', $communityAnswerApprovals->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityAnswerApprovals', 'action' => 'delete', $communityAnswerApprovals->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityAnswerApprovals->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Answer Likes') ?></h4>
        <?php if (!empty($communityAnswer->community_answer_likes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Community Answer Id') ?></th>
                <th scope="col"><?= __('Like Dislike') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($communityAnswer->community_answer_likes as $communityAnswerLikes): ?>
            <tr>
                <td><?= h($communityAnswerLikes->id) ?></td>
                <td><?= h($communityAnswerLikes->community_answer_id) ?></td>
                <td><?= h($communityAnswerLikes->like_dislike) ?></td>
                <td><?= h($communityAnswerLikes->user_id) ?></td>
                <td><?= h($communityAnswerLikes->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityAnswerLikes', 'action' => 'view', $communityAnswerLikes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityAnswerLikes', 'action' => 'edit', $communityAnswerLikes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityAnswerLikes', 'action' => 'delete', $communityAnswerLikes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communityAnswerLikes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Community Answers') ?></h4>
        <?php if (!empty($communityAnswer->child_community_answers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Community Id') ?></th>
                <th scope="col"><?= __('Community Question Id') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Answer') ?></th>
                <th scope="col"><?= __('Total Likes') ?></th>
                <th scope="col"><?= __('Total Dislikes') ?></th>
                <th scope="col"><?= __('Approved') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($communityAnswer->child_community_answers as $childCommunityAnswers): ?>
            <tr>
                <td><?= h($childCommunityAnswers->id) ?></td>
                <td><?= h($childCommunityAnswers->user_id) ?></td>
                <td><?= h($childCommunityAnswers->community_id) ?></td>
                <td><?= h($childCommunityAnswers->community_question_id) ?></td>
                <td><?= h($childCommunityAnswers->parent_id) ?></td>
                <td><?= h($childCommunityAnswers->answer) ?></td>
                <td><?= h($childCommunityAnswers->total_likes) ?></td>
                <td><?= h($childCommunityAnswers->total_dislikes) ?></td>
                <td><?= h($childCommunityAnswers->approved) ?></td>
                <td><?= h($childCommunityAnswers->modified) ?></td>
                <td><?= h($childCommunityAnswers->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CommunityAnswers', 'action' => 'view', $childCommunityAnswers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CommunityAnswers', 'action' => 'edit', $childCommunityAnswers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CommunityAnswers', 'action' => 'delete', $childCommunityAnswers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childCommunityAnswers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
