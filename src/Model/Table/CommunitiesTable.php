<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Communities Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Categories
 * @property \Cake\ORM\Association\HasMany $CommunityAnswers
 * @property \Cake\ORM\Association\HasMany $CommunityBlocks
 * @property \Cake\ORM\Association\HasMany $CommunityInvites
 * @property \Cake\ORM\Association\HasMany $CommunityQuestions
 * @property \Cake\ORM\Association\HasMany $CommunityReports
 * @property \Cake\ORM\Association\HasMany $CommunityReviews
 * @property \Cake\ORM\Association\HasMany $CommunitySearchKeywords
 * @property \Cake\ORM\Association\HasMany $CommunityUsers
 *
 * @method \App\Model\Entity\Community get($primaryKey, $options = [])
 * @method \App\Model\Entity\Community newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Community[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Community|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Community patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Community[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Community findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CommunitiesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('communities');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CommunityCategories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('CommunityAnswers', [
            'foreignKey' => 'community_id'
        ]);
        $this->hasMany('CommunityBlocks', [
            'foreignKey' => 'community_id'
        ]);
        $this->hasMany('CommunityInvites', [
            'foreignKey' => 'community_id'
        ]);
        $this->hasMany('CommunityQuestions', [
            'foreignKey' => 'community_id'
        ]);
        $this->hasMany('CommunityReports', [
            'foreignKey' => 'community_id'
        ]);
        $this->hasMany('CommunityReviews', [
            'foreignKey' => 'community_id'
        ]);
        $this->hasMany('CommunitySearchKeywords', [
            'foreignKey' => 'community_id'
        ]);
        $this->hasMany('CommunityUsers', [
            'foreignKey' => 'community_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->requirePresence('slug', 'create')
            ->notEmpty('slug');

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->requirePresence('short_description', 'create')
            ->notEmpty('short_description');

        $validator
            ->requirePresence('rules', 'create')
            ->allowEmpty('rules');

        $validator
            ->requirePresence('logo', 'create')
            ->allowEmpty('logo');

        $validator
            ->requirePresence('banner', 'create')
            ->allowEmpty('banner');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->integer('rate')
            ->requirePresence('rate', 'create')
            ->allowEmpty('rate');

        $validator
            ->integer('type')
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['category_id'], 'CommunityCategories'));

        return $rules;
    }
}
