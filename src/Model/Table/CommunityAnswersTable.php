<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CommunityAnswers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Communities
 * @property \Cake\ORM\Association\BelongsTo $CommunityQuestions
 * @property \Cake\ORM\Association\BelongsTo $ParentCommunityAnswers
 * @property \Cake\ORM\Association\HasMany $CommunityAnswerApprovals
 * @property \Cake\ORM\Association\HasMany $CommunityAnswerLikes
 * @property \Cake\ORM\Association\HasMany $ChildCommunityAnswers
 *
 * @method \App\Model\Entity\CommunityAnswer get($primaryKey, $options = [])
 * @method \App\Model\Entity\CommunityAnswer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CommunityAnswer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CommunityAnswer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CommunityAnswer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CommunityAnswer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CommunityAnswer findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CommunityAnswersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('community_answers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Communities', [
            'foreignKey' => 'community_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CommunityQuestions', [
            'foreignKey' => 'community_question_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ParentCommunityAnswers', [
            'className' => 'CommunityAnswers',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('CommunityAnswerApprovals', [
            'foreignKey' => 'community_answer_id'
        ]);
        $this->hasMany('CommunityAnswerLikes', [
            'foreignKey' => 'community_answer_id'
        ]);
        $this->hasMany('ChildCommunityAnswers', [
            'className' => 'CommunityAnswers',
            'foreignKey' => 'parent_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('answer', 'create')
            ->notEmpty('answer');

        $validator
            ->requirePresence('total_likes', 'create')
            ->notEmpty('total_likes');

        $validator
            ->requirePresence('total_dislikes', 'create')
            ->notEmpty('total_dislikes');

        $validator
            ->integer('approved')
            ->requirePresence('approved', 'create')
            ->notEmpty('approved');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['community_id'], 'Communities'));
        $rules->add($rules->existsIn(['community_question_id'], 'CommunityQuestions'));
        $rules->add($rules->existsIn(['parent_id'], 'ParentCommunityAnswers'));

        return $rules;
    }
}
