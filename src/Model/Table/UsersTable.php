<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\HasMany $Communities
 * @property \Cake\ORM\Association\HasMany $CommunityAnswerApprovals
 * @property \Cake\ORM\Association\HasMany $CommunityAnswerLikes
 * @property \Cake\ORM\Association\HasMany $CommunityAnswers
 * @property \Cake\ORM\Association\HasMany $CommunityBlocks
 * @property \Cake\ORM\Association\HasMany $CommunityInvites
 * @property \Cake\ORM\Association\HasMany $CommunityQuestions
 * @property \Cake\ORM\Association\HasMany $CommunityQuestionsLikes
 * @property \Cake\ORM\Association\HasMany $CommunityReports
 * @property \Cake\ORM\Association\HasMany $CommunityReviews
 * @property \Cake\ORM\Association\HasMany $CommunityUsers
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');

        $this->hasMany('Communities', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('CommunityAnswerApprovals', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('CommunityAnswerLikes', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('CommunityAnswers', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('CommunityBlocks', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('CommunityInvites', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('CommunityQuestions', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('CommunityQuestionsLikes', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('CommunityReports', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('CommunityReviews', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('CommunityUsers', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('ID', 'create');

        $validator
            ->requirePresence('user_login', 'create')
            ->notEmpty('user_login');

        $validator
            ->requirePresence('user_pass', 'create')
            ->notEmpty('user_pass');

        $validator
            ->requirePresence('user_nicename', 'create')
            ->notEmpty('user_nicename');

        $validator
            ->requirePresence('user_email', 'create')
            ->notEmpty('user_email');

        $validator
            ->requirePresence('user_url', 'create')
            ->notEmpty('user_url');

        $validator
            ->dateTime('user_registered')
            ->requirePresence('user_registered', 'create')
            ->notEmpty('user_registered');

        $validator
            ->requirePresence('user_activation_key', 'create')
            ->notEmpty('user_activation_key');

        $validator
            ->integer('user_status')
            ->requirePresence('user_status', 'create')
            ->notEmpty('user_status');

        $validator
            ->requirePresence('display_name', 'create')
            ->notEmpty('display_name');

        $validator
            ->allowEmpty('sub_domain')
            ->add('sub_domain', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->boolean('subdomain_status')
            ->requirePresence('subdomain_status', 'create')
            ->notEmpty('subdomain_status');

        $validator
            ->integer('reg_type')
            ->requirePresence('reg_type', 'create')
            ->notEmpty('reg_type');

        $validator
            ->requirePresence('mobile', 'create')
            ->notEmpty('mobile');

        $validator
            ->requirePresence('add1', 'create')
            ->notEmpty('add1');

        $validator
            ->requirePresence('add2', 'create')
            ->notEmpty('add2');

        $validator
            ->date('dob')
            ->requirePresence('dob', 'create')
            ->notEmpty('dob');

        $validator
            ->requirePresence('about', 'create')
            ->notEmpty('about');

        $validator
            ->requirePresence('profession', 'create')
            ->notEmpty('profession');

        $validator
            ->requirePresence('user_img', 'create')
            ->notEmpty('user_img');

        $validator
            ->requirePresence('father_name', 'create')
            ->notEmpty('father_name');

        $validator
            ->integer('pin')
            ->requirePresence('pin', 'create')
            ->notEmpty('pin');

        $validator
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        $validator
            ->requirePresence('country', 'create')
            ->notEmpty('country');

        $validator
            ->requirePresence('state', 'create')
            ->notEmpty('state');

        $validator
            ->requirePresence('city', 'create')
            ->notEmpty('city');

        $validator
            ->requirePresence('recover_pass', 'create')
            ->notEmpty('recover_pass');

        $validator
            ->requirePresence('month_year', 'create')
            ->notEmpty('month_year');

        $validator
            ->requirePresence('reg_loc', 'create')
            ->notEmpty('reg_loc');

        $validator
            ->allowEmpty('ip_address');

        $validator
            ->dateTime('modifieddate')
            ->allowEmpty('modifieddate');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['sub_domain']));

        return $rules;
    }
}
