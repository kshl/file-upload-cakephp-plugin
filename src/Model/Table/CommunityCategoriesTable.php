<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CommunityCategories Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ParentCommunityCategories
 * @property \Cake\ORM\Association\HasMany $ChildCommunityCategories
 * @property \Cake\ORM\Association\HasMany $CommunityCategoryMappings
 *
 * @method \App\Model\Entity\CommunityCategory get($primaryKey, $options = [])
 * @method \App\Model\Entity\CommunityCategory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CommunityCategory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CommunityCategory|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CommunityCategory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CommunityCategory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CommunityCategory findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CommunityCategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('community_categories');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ParentCommunityCategories', [
            'className' => 'CommunityCategories',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildCommunityCategories', [
            'className' => 'CommunityCategories',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('CommunityCategoryMappings', [
            'foreignKey' => 'community_category_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->requirePresence('cat_name', 'create')
            ->notEmpty('cat_name');

        $validator
            ->requirePresence('short_description', 'create')
            ->notEmpty('short_description');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentCommunityCategories'));

        return $rules;
    }
}
