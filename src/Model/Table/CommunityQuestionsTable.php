<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CommunityQuestions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Communities
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $CommunityAnswers
 * @property \Cake\ORM\Association\HasMany $CommunityQuestionFiles
 * @property \Cake\ORM\Association\HasMany $CommunityQuestionKeywords
 * @property \Cake\ORM\Association\HasMany $CommunityQuestionsLikes
 *
 * @method \App\Model\Entity\CommunityQuestion get($primaryKey, $options = [])
 * @method \App\Model\Entity\CommunityQuestion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CommunityQuestion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CommunityQuestion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CommunityQuestion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CommunityQuestion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CommunityQuestion findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CommunityQuestionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('community_questions');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Communities', [
            'foreignKey' => 'community_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('CommunityAnswers', [
            'foreignKey' => 'community_question_id'
        ]);
        $this->hasMany('CommunityQuestionFiles', [
            'foreignKey' => 'community_question_id'
        ]);
        $this->hasMany('CommunityQuestionKeywords', [
            'foreignKey' => 'community_question_id'
        ]);
        $this->hasMany('CommunityQuestionsLikes', [
            'foreignKey' => 'community_question_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->integer('total_comments')
            ->requirePresence('total_comments', 'create')
            ->notEmpty('total_comments');

        $validator
            ->requirePresence('total_views', 'create')
            ->notEmpty('total_views');

        $validator
            ->requirePresence('total_likes', 'create')
            ->notEmpty('total_likes');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['community_id'], 'Communities'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
