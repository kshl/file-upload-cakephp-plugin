<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CommunityCategory Entity
 *
 * @property int $id
 * @property int $parent_id
 * @property string $title
 * @property string $cat_name
 * @property string $short_description
 * @property int $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\ParentCommunityCategory $parent_community_category
 * @property \App\Model\Entity\ChildCommunityCategory[] $child_community_categories
 * @property \App\Model\Entity\CommunityCategoryMapping[] $community_category_mappings
 */
class CommunityCategory extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
