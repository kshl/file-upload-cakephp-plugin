<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CommunityQuestion Entity
 *
 * @property int $id
 * @property int $community_id
 * @property int $user_id
 * @property string $title
 * @property string $description
 * @property int $status
 * @property int $total_comments
 * @property int $total_views
 * @property int $total_likes
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Community $community
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\CommunityAnswer[] $community_answers
 * @property \App\Model\Entity\CommunityQuestionFile[] $community_question_files
 * @property \App\Model\Entity\CommunityQuestionKeyword[] $community_question_keywords
 * @property \App\Model\Entity\CommunityQuestionsLike[] $community_questions_likes
 */
class CommunityQuestion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
