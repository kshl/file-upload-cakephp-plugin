<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Community Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $short_description
 * @property string $rules
 * @property string $logo
 * @property string $banner
 * @property int $status
 * @property int $category_id
 * @property int $rate
 * @property int $type
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\CommunityAnswer[] $community_answers
 * @property \App\Model\Entity\CommunityBlock[] $community_blocks
 * @property \App\Model\Entity\CommunityInvite[] $community_invites
 * @property \App\Model\Entity\CommunityQuestion[] $community_questions
 * @property \App\Model\Entity\CommunityReport[] $community_reports
 * @property \App\Model\Entity\CommunityReview[] $community_reviews
 * @property \App\Model\Entity\CommunitySearchKeyword[] $community_search_keywords
 * @property \App\Model\Entity\CommunityUser[] $community_users
 */
class Community extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
