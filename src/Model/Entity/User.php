<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $ID
 * @property string $user_login
 * @property string $user_pass
 * @property string $user_nicename
 * @property string $user_email
 * @property string $user_url
 * @property \Cake\I18n\Time $user_registered
 * @property string $user_activation_key
 * @property int $user_status
 * @property string $display_name
 * @property string $sub_domain
 * @property bool $subdomain_status
 * @property int $reg_type
 * @property int $mobile
 * @property string $add1
 * @property string $add2
 * @property \Cake\I18n\Time $dob
 * @property string $about
 * @property string $profession
 * @property string $user_img
 * @property string $father_name
 * @property int $pin
 * @property string $last_name
 * @property int $country
 * @property int $state
 * @property int $city
 * @property string $recover_pass
 * @property string $month_year
 * @property string $reg_loc
 * @property string $ip_address
 * @property \Cake\I18n\Time $modifieddate
 *
 * @property \App\Model\Entity\PackagePurchase $package_purchase
 * @property \App\Model\Entity\Community[] $communities
 * @property \App\Model\Entity\CommunityAnswerApproval[] $community_answer_approvals
 * @property \App\Model\Entity\CommunityAnswerLike[] $community_answer_likes
 * @property \App\Model\Entity\CommunityAnswer[] $community_answers
 * @property \App\Model\Entity\CommunityBlock[] $community_blocks
 * @property \App\Model\Entity\CommunityInvite[] $community_invites
 * @property \App\Model\Entity\CommunityQuestion[] $community_questions
 * @property \App\Model\Entity\CommunityQuestionsLike[] $community_questions_likes
 * @property \App\Model\Entity\CommunityReport[] $community_reports
 * @property \App\Model\Entity\CommunityReview[] $community_reviews
 * @property \App\Model\Entity\CommunityUser[] $community_users
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'ID' => false
    ];
}
