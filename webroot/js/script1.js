( function( $ ) {
$( document ).ready(function() {
$('#cssmenu li.has-sub>span').on('click', function(){
		//$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp();
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown();
			element.siblings('li').children('ul').slideUp();
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp();
		}
		    $('.menulvl1 >span .holder').addClass('holder-h1');
    $('.menulvl2 >span .holder1').addClass('holder-h2');
    $('.menulvl3 >span .holder2').addClass('holder-h3');
	$('.menulvl1.open > span .holder').removeClass('holder-h1');
	$('.menulvl2.open > span .holder1').removeClass('holder-h2');
	$('.menulvl3.open > span .holder2').removeClass('holder-h3');
	});
	function rgbToHsl(r, g, b) {
	    r /= 255, g /= 255, b /= 255;
	    var max = Math.max(r, g, b), min = Math.min(r, g, b);
	    var h, s, l = (max + min) / 2;

	    if(max == min){
	        h = s = 0;
	    }
	    else {
	        var d = max - min;
	        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
	        switch(max){
	            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
	            case g: h = (b - r) / d + 2; break;
	            case b: h = (r - g) / d + 4; break;
	        }
	        h /= 6;
	    }
	    return l;
	}
});
} )( jQuery );
