Notify = function(text, callback, close_callback, style) {

	var time = '5000';
	var $container = $('#notifications-alert');
	var icon = '<i class="fa fa-info-circle alert-icon-bg"></i>';
 
	if (typeof style == 'undefined' ) style = 'warning'
  
	var html = $('<div class="alert-custom alert-validatin hide">' + icon +  " " + text + '</div>');
  
	$('<a>',{
		text: '×',
		class: 'button alert-custom-close',
		style: 'padding-left: 20px;',
		href: '#',
		click: function(e){
			e.preventDefault()
			close_callback && close_callback()
			remove_notice()
		}
	}).prependTo(html)

	$container.html(html)
	html.removeClass('hide').hide().fadeIn('slow')

	function remove_notice() {
		html.stop().fadeOut('slow').remove()
	}
	
	var timer =  setInterval(remove_notice, time);

	$(html).hover(function(){
		clearInterval(timer);
	}, function(){
		timer = setInterval(remove_notice, time);
	});
	
	html.on('click', function () {
		clearInterval(timer)
		callback && callback()
		remove_notice()
	});
  
  
}
