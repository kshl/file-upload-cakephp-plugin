var UploadCount=0;
var TotalUploadCount=0;
var ProcessCompleteCount = 0;
var group = false;
var previousHTML;
var SelectID;
var Videowhizz_APP_URL = 'https://videowhizz.saglus.com/videowhizz_server/sendjson/';

var ProjectID = 188;
var APIkey = 'e784ea1e3e2afa3a33e563141274b2da';

function ID(el){
	return document.getElementById(el);
}
function ClassName(el){
	return document.getElementsByClassName(el);
}
	
function appenduploads(SelectedID){
	SelectID = SelectedID;
	
	//console.log(SelectedID.files[0].name);
	
	var imageType = ['avi', 'divx', 'flv', 'm4v', 'mkv', 'mov', 'mp4', 'mpeg', 'mpg', 'ogm', 'ogv', 'ogx', 'rm', 'rmvb', 'smil', 'webm', 'wmv', 'xvid'];  
	
	for(var temp=0; temp < SelectID.files.length; temp++){
			var FileName = SelectID.files[temp].name;
			var FileExtension = FileName.substr( (FileName.lastIndexOf('.') +1) );
			
			//console.log(FileExtension);
		if (-1 == $.inArray(FileExtension, imageType)){
		  alert("not valid");
		  return false;
		}
	}
		

	TotalUploadCount = SelectID.files.length;
	
	for (var temp = 0; temp < SelectID.files.length; temp++) {
		
		$("#videouploaderdiv")
			.append($('<div>')
			.attr('class', 'col-md-12 padding0 mt2')
				.append($('<div>')
				.attr('class', 'col-md-2 col-lg-1 col-sm-2 col-xs-12 padding0')
					.append($('<img>')
					.attr('src', 'https://eduncle-cdn.saglus.com/webfiles/images/ajax-load.gif')
					.attr('class', 'img-responsive padding0 col-xs-3 col-xs-offset-4 col-md-offset-0 col-sm-offset-0 col-lg-offset-0 col-md-10 col-sm-11 col-lg-11')
					.attr('id', 'videoupload_thumb'+temp)
					)
				)
				.append($('<div>')
				.attr('class', 'col-lg-9 col-md-7 col-sm-7 col-xs-12 vmt1 xsmt2')
					.append($('<p>'+SelectID.files[temp].name+'</p>').attr('id', 'videoupload_title'+temp))
					.append($('<div>')
					.attr('class', 'progress progress-bar_format')
						.append($('<div>')
						.attr('class', 'progress-bar progress-bar-striped active')
						.attr('id', 'videoupload_percent_width'+temp)
						.attr('style', 'width: 0%')
							.append($('<span>0% Complete</span>')
							.attr('id', 'videoupload_percent_text'+temp)
							)
						)
					)
				)
				.append($('<input>')
				.attr('id', 'GetNewVideoName'+temp)	
				.attr('type', 'hidden')	
				)
				.append($('<div>')
				.attr('class', 'clearfix')		
				)
			
			);
	}
	StartUploads();
}

function StartUploads(){
	file = SelectID.files[UploadCount];
	//ID("GetTitleID"+UploadCount).value=file.name;
	SingleUploads(file);
}

var blob, BYTES_PER_CHUNK, SIZE, NUM_CHUNKS, start, end;
var newVideoName = '';
var current_file_part_no=1;

function SingleUploads_hold(file){
	var formdata = new FormData();
	formdata.append('video', file);
	formdata.append("Action", 'AddVideo');
	formdata.append("ProjectID", ProjectID);
	formdata.append("APIkey", APIkey);
	var ajax = new XMLHttpRequest();
	ajax.upload.addEventListener("progress", UploadHandler, false);
	ajax.addEventListener("load", function(event){UploadComplete(event);}, false);
	ajax.open("POST", Videowhizz_APP_URL);
	ajax.send(formdata);
	//console.log(formdata);
}

function SingleUploads(file){
	blob = file;
	BYTES_PER_CHUNK = parseInt(1048576*1, 10);
	SIZE = blob.size;
	NUM_CHUNKS = Math.max(Math.ceil(SIZE / BYTES_PER_CHUNK), 1);
	start = 0;
	end = BYTES_PER_CHUNK;
	
	uploadnext();
}

function uploadnext(event=''){
	//console.log(SelectID.files.length+"uploadnext");
	if(start < SIZE){
		
		if(event!=''){
			newVideoName = JSON.parse(event.target.responseText).VideoName;
			current_file_part_no++;
		}
		
		var percent = (((current_file_part_no-1)/ NUM_CHUNKS) * 100).toFixed(1);
		percent = Math.round(percent - 0.5);
		// ID("videoupload_percent_width"+UploadCount).value = Math.round(percent);
		$("#videoupload_percent_width"+UploadCount).css("width", percent+"%");
		$("#videoupload_percent_text"+UploadCount).html(percent+"% upload complete");
		
		upload(blob.slice(start, end));
		start = end;
		end = start + BYTES_PER_CHUNK;
	}else{
		
		current_file_part_no = 1;
		newVideoName = JSON.parse(event.target.responseText).VideoName;
		StartVideoProcess(newVideoName, UploadCount);
		
		newVideoName = '';
		UploadCount++;
		
		
		if(UploadCount < SelectID.files.length){
			StartUploads(ProjectID, SelectID);
		}
	}
	
}
function upload(blobOrFile) {
	//console.log(SelectID.files.length+"upload");
				var formdata = new FormData();
				if(newVideoName!=''){
					formdata.append('VideoName',newVideoName);
				}
				else{
					formdata.append('file_ext','mp4');
				}
				formdata.append('APIkey',APIkey);
				formdata.append('Action','AddVideopart');
				formdata.append('filedata',blobOrFile);
				
				var ajax = new XMLHttpRequest();
				ajax.upload.addEventListener("progress", UploadHandler, false);
				ajax.addEventListener("load", function(event){uploadnext(event);}, false);
				//ajax.open("POST", 'http://testing.localhost/videowhizz/videomaker');
				ajax.open("POST", Videowhizz_APP_URL);
				ajax.send(formdata);
	  }

function UploadHandler(event){ //process progress bar

	var percent_part = (event.loaded / event.total / NUM_CHUNKS) * 100;
	percent_part = Math.round(percent_part);
	
	var percent = (((current_file_part_no-1)/ NUM_CHUNKS) * 100).toFixed(1);
	percent = Math.round(percent) + percent_part;
	percent = Math.round(percent - 0.5);
	$("#videoupload_percent_width"+UploadCount).css("width", percent+"%");
	$("#videoupload_percent_text"+UploadCount).html(percent+"% upload complete");
		
}

function UploadComplete(event, ProjectID, SelectID){  //afert file upload

	console.log(event.target.responseText);
	VideoName = JSON.parse(event.target.responseText).NewFileName;
	ID("GetUploadID"+UploadCount).value = VideoName;
	ID("VideoStatusTextID"+UploadCount).innerHTML= "Video is Processing";
	ID("progressBar"+UploadCount).value = 0;
	StartCheckProcessStatus(VideoName,UploadCount);
	
	
	UploadCount++;
	if(UploadCount < SelectID.files.length){
		StartUploads(ProjectID, SelectID);
	}
	
}


function StartVideoProcess(VideoName, UploadID){
	//ID("VideoStatusTextID"+UploadID).innerHTML= "Video is Processing";

	
	ID("GetNewVideoName"+UploadID).value = VideoName;
	console.log('StartVideoProcess:'+VideoName);
	$.post(Videowhizz_APP_URL,
	{
		'APIkey': APIkey,
		'Action': 'StartVideoProcess',
		'VideoName': VideoName,
		'Video_Title': SelectID.files[UploadID].name,
		'ProjectID': ProjectID,
	},
	function(data, status){
			console.log(VideoName);
			
			ID("videoupload_thumb"+UploadID).src = 'https://videowhizz.saglus.com/videowhizz_server/uploads/'+VideoName.split(".")[0]+'/'+VideoName.split(".")[0]+'.png';
			
			uploadFile_video(VideoName);
			UploadDoneAll();
			
			// StartCheckProcessStatus(VideoName, UploadID)
	});
}
function StartCheckProcessStatus(VideoName, UploadID){
	var ProcessID = UploadID;

	console.log("t="+TotalUploadCount+'p='+ProcessCompleteCount);

	$.post(Videowhizz_APP_URL,
	{
		'APIkey': APIkey,
		'Action': 'getProcessStatus',
		'VideoName': VideoName,
	},
	function(data, status){
			
			ProcessStatus = JSON.parse(data);
			percent = ProcessStatus.getProcessStatus.toFixed(1);
			percent = Math.round(percent - 0.5);
			$("#videoupload_percent_width"+UploadID).css("width", percent+"%");
			$("#videoupload_percent_text"+UploadID).html(percent+"% Video is processed");
		
			//alert(data);
			if(ProcessStatus.getProcessStatus!=100){
				StartCheckProcessStatus(VideoName, ProcessID);
			}else{
				MoveVideoOnS3(VideoName);
				ProcessCompleteCount++;
			}
	});
}
function MoveVideoOnS3(VideoName){
$("#VideoNameinput").val(VideoName.split(".")[0]);

	$.post(Videowhizz_APP_URL,
	{
		'APIkey': APIkey,
		'Action': 'MoveVideoOnS3',
		'VideoName': VideoName,
	},
	function(data, status){
			uploadFile_video(VideoName);
			UploadDoneAll();
	});


//call ajax to save video id in DB	
}

/*function savevideoIDinDB(VideoName){
	//alert(VideoName);
	var formdata = new FormData($("#video_form")[0]);
	formdata.append('VideoName',VideoName);
	console.log(formdata);
	var ajax = new XMLHttpRequest();
	ajax.addEventListener("load", function(event){uploadnext(event);}, false);
	//alert(document.location.pathname.match(/[^\/]+$/)[0]);
	ajax.open("POST", 'library_video_upload');
	ajax.send(formdata);
}*/

/*$(document).ready(function(){
    savevideoIDinDB("ssad");
});*/

function UploadDoneAll(){
	if(UploadCount==TotalUploadCount){
		//AlertAction("<strong>Success!</strong> Video(s) is Uploaded.");
		addvideodone(TotalUploadCount);
	}
}
function addvideodone(videocount){
	$(".error_success").show();
	$(".error_success-text").html("Video Add successfully");

	setTimeout(function(){
		//location.reload();
		return true;
	},5000);
}