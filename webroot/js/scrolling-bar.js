// $(document).ready(function(){
//    $(window).bind('scroll', function() {
//     var navHeight = $( window ).height() - 0;
//          if ($(window).scrollTop() > navHeight) {
//              $('.sticky-bar-pd').addClass('fixed');                 
//              $('#sticky-btn').slideDown(200);
//          }
//          else {
//              $('.sticky-bar-pd').removeClass('fixed');
//              $('#sticky-btn').slideUp(200);
//          }
//     });
// });

$(document).ready(function () {
    $(document).on("scroll", onScroll);
    
    // Script for smoothe scroll
    $('.page-scroll').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");
        
        $('.page-scroll').each(function () {
            $(this).removeClass('arrow-active');
        })
        $(this).addClass('arrow-active');
      
        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top-70
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });
});


function onScroll(event){
    var scrollPos = $(document).scrollTop()-600;
    $('.pd-links a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('.pd-links a').removeClass("arrow-active");
            currLink.addClass("arrow-active");
        }
        else{
            currLink.removeClass("arrow-active");
        }
    });
}


$(document).ready(function(){
    $(window).scroll(function () {
      if ( $(this).scrollTop() > 500 && !$('.sticky-bar-pd').hasClass('open') ) {
            $('.sticky-bar-pd').addClass('open');
            $('.sticky-bar-pd').slideDown();
        } else if ( $(this).scrollTop() <= 500 ) {            
            $('.sticky-bar-pd').removeClass('open');
            $('.sticky-bar-pd').slideUp();

      }
    });


    $("#closeScrollingBar").click(function(){
        // if($("#mocktest_popup").show()){
        //     $("html").css("overflow", "hidden");
        // }else{
        //     $("html").css("overflow", "auto");
        // }
        if($('.sticky-bar-pd').hasClass('open')){
            $('.sticky-bar-pd').removeClass('open');
            $('.sticky-bar-pd').slideUp();
        }else{
            $('.sticky-bar-pd').addClass('open');
            $('.sticky-bar-pd').slideDown(); 
        }
    });
    $("#exit_popup").click(function(){
        $(".sticky-bar-pd").slideDown();
    });

    
});