
var ww = document.body.clientWidth;

$(document).ready(function() {
	$(".nav_sag li a").each(function() {
		if ($(this).next().length > 0) {
			$(this).addClass("inner-parent");
		};
	})
	
	$(".toggleMenu").click(function(e) {
		e.preventDefault();
		$(this).toggleClass("active");
		$(".nav_sag").toggle();
	});
	adjustMenu();
})

$(window).bind('resize orientationchange', function() {
	ww = document.body.clientWidth;
	adjustMenu();
});

var adjustMenu = function() {
	if (ww < 768) {
		$(".toggleMenu").css("display", "inline-block");
		if (!$(".toggleMenu").hasClass("active")) {
			$(".nav_sag").hide();
		} else {
			$(".nav_sag").show();
		}
		$(".nav_sag li").unbind('mouseenter mouseleave');
		$(".nav_sag li a.inner-parent").unbind('click').bind('click', function(e) {
			// must be attached to anchor element to prevent bubbling
			e.preventDefault();
			$(this).parent("li").toggleClass("hover");
		});
	} 
	else if (ww >= 768) {
		$(".toggleMenu").css("display", "none");
		$(".nav_sag").show();
		$(".nav_sag li").removeClass("hover");
		$(".nav_sag li a").unbind('click');
		$(".nav_sag li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
		 	// must be attached to li so that mouseleave is not triggered when hover over submenu
		 	$(this).toggleClass('hover');
		});
	}
}

