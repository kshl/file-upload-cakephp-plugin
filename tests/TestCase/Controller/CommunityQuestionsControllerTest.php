<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CommunityQuestionsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CommunityQuestionsController Test Case
 */
class CommunityQuestionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.community_questions',
        'app.communities',
        'app.users',
        'app.community_answer_approvals',
        'app.community_answer_likes',
        'app.community_answers',
        'app.community_blocks',
        'app.community_invites',
        'app.community_questions_likes',
        'app.community_reports',
        'app.community_reviews',
        'app.community_users',
        'app.community_categories',
        'app.community_category_mappings',
        'app.community_search_keywords',
        'app.community_question_files',
        'app.community_question_keywords'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
