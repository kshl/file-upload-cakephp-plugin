<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\FileNameHandlingComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\FileNameHandlingComponent Test Case
 */
class FileNameHandlingComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\FileNameHandlingComponent
     */
    public $FileNameHandling;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->FileNameHandling = new FileNameHandlingComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FileNameHandling);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
