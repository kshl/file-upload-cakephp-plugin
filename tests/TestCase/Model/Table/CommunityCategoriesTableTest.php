<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CommunityCategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CommunityCategoriesTable Test Case
 */
class CommunityCategoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CommunityCategoriesTable
     */
    public $CommunityCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.community_categories',
        'app.community_category_mappings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CommunityCategories') ? [] : ['className' => 'App\Model\Table\CommunityCategoriesTable'];
        $this->CommunityCategories = TableRegistry::get('CommunityCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CommunityCategories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
