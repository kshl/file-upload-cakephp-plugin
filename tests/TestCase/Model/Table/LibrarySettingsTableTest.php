<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LibrarySettingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LibrarySettingsTable Test Case
 */
class LibrarySettingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LibrarySettingsTable
     */
    public $LibrarySettings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.library_settings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LibrarySettings') ? [] : ['className' => 'App\Model\Table\LibrarySettingsTable'];
        $this->LibrarySettings = TableRegistry::get('LibrarySettings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LibrarySettings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
