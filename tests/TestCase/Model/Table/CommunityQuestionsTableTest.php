<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CommunityQuestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CommunityQuestionsTable Test Case
 */
class CommunityQuestionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CommunityQuestionsTable
     */
    public $CommunityQuestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.community_questions',
        'app.communities',
        'app.users',
        'app.community_answer_approvals',
        'app.community_answer_likes',
        'app.community_answers',
        'app.community_blocks',
        'app.community_invites',
        'app.community_questions_likes',
        'app.community_reports',
        'app.community_reviews',
        'app.community_users',
        'app.community_categories',
        'app.community_category_mappings',
        'app.community_search_keywords',
        'app.community_question_files',
        'app.community_question_keywords'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CommunityQuestions') ? [] : ['className' => 'App\Model\Table\CommunityQuestionsTable'];
        $this->CommunityQuestions = TableRegistry::get('CommunityQuestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CommunityQuestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
