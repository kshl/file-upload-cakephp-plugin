<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CommunityAnswersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CommunityAnswersTable Test Case
 */
class CommunityAnswersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CommunityAnswersTable
     */
    public $CommunityAnswers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.community_answers',
        'app.users',
        'app.communities',
        'app.community_categories',
        'app.community_category_mappings',
        'app.community_blocks',
        'app.community_invites',
        'app.community_questions',
        'app.community_question_files',
        'app.community_question_keywords',
        'app.community_questions_likes',
        'app.community_reports',
        'app.community_reviews',
        'app.community_search_keywords',
        'app.community_users',
        'app.community_answer_approvals',
        'app.community_answer_likes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CommunityAnswers') ? [] : ['className' => 'App\Model\Table\CommunityAnswersTable'];
        $this->CommunityAnswers = TableRegistry::get('CommunityAnswers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CommunityAnswers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
